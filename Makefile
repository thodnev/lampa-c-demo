CC = gcc
CFLAGS = -O2 -Wall -Wextra -Wpedantic
SRCDIR = src
BUILDDIR = build
PROGNAME = main

.PHONY: all clean format

all: $(BUILDDIR)/$(PROGNAME)

$(BUILDDIR)/$(PROGNAME): $(BUILDDIR)/main.o
	$(CC) $(CFLAGS) $^ -o $@

$(BUILDDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) $^ -o $@

$(BUILDDIR):
	mkdir -p $(BUILDDIR)

clean:
	rm -rf $(BUILDDIR)

format: $(wildcard $(SRCDIR)/*.c)
	clang-format -style=file -i -verbose $^
