#include <argp.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct message {
	char *data;
	unsigned long long len;
};

struct args {
	int port;
	char ip_addr[16];
	unsigned long long maxlen;
	unsigned long long maxnmsg;
};

struct args args = {
	.port = 9000,
	.ip_addr = "127.0.0.1",
	.maxlen = 20,
	.maxnmsg = 20
};

const struct argp_option options[] = {
	{"port", 'p', "PORT", OPTION_ARG_OPTIONAL, "Port on which the server is running", 0},
	{"ip", 'i', "ADDR", OPTION_ARG_OPTIONAL, "IP address of the server (IPv4 only)", 0},
	{"mlen", 'm', "MSYM", OPTION_ARG_OPTIONAL, "Maximum amount of chars in message", 0},
	{"nmsg", 'n', "NMSG", OPTION_ARG_OPTIONAL, "Maximum amount of messages stored", 0},
	{0}
};

const char *const docstr = "A simple echo server in C";


/* TODO: refactor */
error_t parser (int key, char *arg, struct argp_state *state)
{
	char *val = !arg ? state->argv[state->next] : arg;

	switch (key) {
	case 'p':
		args.port = atoi(val);
		break;
	case 'i':
		strncpy(args.ip_addr, val, sizeof(args.ip_addr)/sizeof(*args.ip_addr)-1);
		break;
	case 'm':
		args.maxlen = atoll(val);
		break;
	case 'n':
		args.maxnmsg = atoll(val);
		break;

    case ARGP_KEY_ARG:     /* see https://tinyurl.com/argp-special-keys  */
		/* Workaround to handle spaces between switches and values */
		if (state->arg_num >= 2)	/* too many args */
        		argp_usage(state);
		break;
	case ARGP_KEY_END:
		break;
	default:
      	return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

const struct argp argp = {
	.options = options,
	.parser = &parser,
	.doc = docstr
};


int main(int argc, char *argv[])
{
	error_t err = argp_parse(&argp, argc, argv, 0, 0, 0);
	if (err) {
		fprintf(stderr, "Error %d occured during parsing", (int)err);
		exit(err);
	}

	printf("Parsed arguments:\n"
		   "\tport = %d\n"
		   "\tip_addr = %s\n"
		   "\tmaxlen = %llu\n"
		   "\tmaxnmsg = %llu\n",
		   args.port, args.ip_addr, args.maxlen, args.maxnmsg);

	return 0;
}